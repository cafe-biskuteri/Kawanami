#!/bin/sh

LIBS="$LIBS:../Hinoki"
FLAGS="-Xlint:unchecked"

if test "$1" = "c"
then
	javac -cp "$LIBS" "$FLAGS" \
		cafe/biskuteri/kawanami/*.java
fi

if test "$1" = "ct"
then
	javac -cp "$LIBS" "$FLAGS" \
		cafe/biskuteri/kawanami/tests/*.java
fi

if test "$1" = "rt"
then
	false java \
		-cp "$LIBS:cafe/biskuteri/kawanami/tests" \
		-ea MapperTests \
		|| exit
fi

