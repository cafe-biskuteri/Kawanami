
package cafe.biskuteri.kawanami;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ThreadFactory;
import java.util.Comparator;
import java.util.Map;
import java.util.List;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.zip.CRC32;


public class
Kawanami implements Comparator<Kawanami.Job>, ThreadFactory {

	private IO
	io;


//  -=- --- -%- --- -=-


	private PriorityBlockingQueue<Job>
	jobQueue;

	private ScheduledExecutorService
	workers;

	private boolean
	crashed;

	private IOServices
	ioServices;


//  -=- --- -%- --- -=-


	private static final TimeUnit
	MILLI = TimeUnit.MILLISECONDS;



// -=- - --%- -%-- - -=-
//        %  @  %
// -=- - --%- -%-- - -=-



	public <Params> Job<Params>
	prepareEmptyJob()
	{
		Job<Params> job = new Job<>();
		job.id = generateID(job);
		job.controller = new JobController<>(job);
		job.status = "PREPARED";
		return job;
	}

	public void
	enqueue(Job<?> job)
	{
		if (!crashed)
		{
			job.status = "QUEUED";
			io.acceptKawanamiLogLine(
				ioServices.makeLogLine(job, job.description));
			jobQueue.add(job);

			io.acceptKawanamiJobInput(
				ioServices.makeJobInput(job));
		}
	}

//   -      -%-      -

	public void
	shutdown()
	{
		workers.shutdown();
	}

	public void
	awaitTermination()
	throws InterruptedException
	{
		while (!workers.isTerminated())
			workers.awaitTermination(100, MILLI);
	}

	public void
	awaitCleared()
	throws InterruptedException
	{
		while (!jobQueue.isEmpty()) MILLI.sleep(100);
	}

//   -      -%-      -

	public IOServices
	getIOServices() { return ioServices; }


//  -=- --- -%- --- -=-


	private String
	generateID(Object object)
	{
		StringBuilder b = new StringBuilder();
		b.append(ZonedDateTime.now().toEpochSecond());
		b.append(this.hashCode());
		b.append(object.hashCode());
		b.append((short)(Math.random() * Short.MAX_VALUE));

		CRC32 summer = new CRC32();
		summer.update(b.toString().getBytes());
		return Long.toString(summer.getValue());
	}


//  -=- --- -%- --- -=-


	public int
	compare(Job job1, Job job2)
	{
		return job2.priority - job1.priority;
	}

	public Thread
	newThread(Runnable r)
	{
		Thread returnee = new Thread(r);
		returnee.setName("Kawanami-" + returnee.getId());
		return returnee;
	}



// -=- - --%- -%-- - -=-
//        %  @  %
// -=- - --%- -%-- - -=-



	public static class
	Job<Params> {

		public Params
		params;

		public String
		id;

		public String
		description;

		public int
		priority;

		public String
		status;

		public JobCode<Params>
		code;

		public ParamsSerialiser<Params>
		serialiser;

		JobController<Params>
		controller;

	}

	public interface
	JobCode<Params> {

		void
		accept(Params params, String jobID)
		throws Exception;

	}

	public interface
	ParamsSerialiser<Params> {

		Map<String, String>
		serialise(Params params);

		Params
		deserialise(Map<String, String> serialParams);

	}

	public interface
	IO {

		void
		acceptKawanamiLogLine(IOServices.LogLine logLine);

		void
		acceptKawanamiJobInput(IOServices.JobInput jobInput);

		List<IOServices.LogLine>
		getKawanamiLog();

		List<IOServices.JobInput>
		getKawanamiJobInputs();

	}


//  -=- --- -%- --- -=-


	private class
	Worker implements Runnable {

		public void
		run()
		{
			if (crashed) return;

			Job<?> job; try { job = jobQueue.take(); }
			catch (InterruptedException eIt) { return; }

			try
			{
				job.controller.inWorkerThread();

				job.status = "FINISHED";
				io.acceptKawanamiLogLine(
					ioServices.makeLogLine(job, job.description));
			}
			catch (Exception e)
			{
				job.status = "FAILED";
				io.acceptKawanamiLogLine(
					ioServices.makeLogLine(
						job,
						e.getClass().getName()
						+ ": " + e.getMessage()));
			}
		}

	}

//   -      -%-      -

	static class
	JobController<Params> {

		private Job<Params>
		job;

//		---%-@-%---

		void
		inWorkerThread()
		throws Exception
		{ job.code.accept(job.params, job.id); }

		Map<String, String>
		serialiseJobParams()
		{ return job.serialiser.serialise(job.params); }

//		---%-@-%---

		private 
		JobController(Job<Params> job) { this.job = job; }

	}



// -=- - --%- -%-- - -=-
//        %  @  %
// -=- - --%- -%-- - -=-



	public
	Kawanami(IO io)
	{
		this.io = io;
		ioServices = new IOServices(this.io);

		jobQueue = new PriorityBlockingQueue<Job>(256, this);

		workers = Executors.newScheduledThreadPool(4, this);
		Runnable worker = new Worker();
		workers.scheduleWithFixedDelay(worker, 0, 100, MILLI);
		workers.scheduleWithFixedDelay(worker, 0, 100, MILLI);
		workers.scheduleWithFixedDelay(worker, 0, 100, MILLI);
		workers.scheduleWithFixedDelay(worker, 0, 100, MILLI);

		crashed = false;
	}

}
