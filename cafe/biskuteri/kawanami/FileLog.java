
package cafe.biskuteri.kawanami;

import java.nio.file.Files;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.time.format.DateTimeFormatter;
import cafe.biskuteri.hinoki.SilentParsers;

public class
FileLog {

	private String
	logFileName;

	private FileWriter
	w;
	
//	---%-@-%---

	public void
	acceptKawanamiLogLine(IOServices.LogLine logLine)
	throws IOException
	{
		w.write(toString(logLine));
		w.flush();
	}

	public List<IOServices.LogLine>
	getKawanamiLog()
	throws IOException
	{
		List<IOServices.LogLine> returnee = new ArrayList<>();
		for (String line: fileToLines(logFileName))
		{
			String[] fields = line.split(" %% ");

			IOServices.LogLine addee = new IOServices.LogLine();
			addee.timestamp = SilentParsers
				.toZonedDateTime(nullGet(fields, 0));
			addee.jobStatus = toStatus(nullGet(fields, 1));
			addee.codeClassName = nullGet(fields, 2);
			addee.jobId = nullGet(fields, 3);
			addee.lineDescription = nullGet(fields, 4);
			returnee.add(addee);
		}
		return returnee;
	}

	void
	close()
	throws IOException
	{ w.close(); }

//	 -	-%-	 -

	private static String
	toString(IOServices.LogLine logLine)
	{
		String timestamp = logLine.timestamp
			.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);

		return timestamp
			+ " %% " + logLine.jobStatus
			+ " %% " + logLine.codeClassName
			+ " %% " + logLine.jobId
			+ " %% " + logLine.lineDescription
			+ "\n";
	}

	private static List<String>
	fileToLines(String filepath)
	throws IOException
	{
		return Files.readAllLines(new File(filepath).toPath());
	}

	private static String
	toStatus(String s)
	{
		if (s == null) return null;
		if (s.equals("PREPARED")) return s;
		if (s.equals("QUEUED")) return s;
		if (s.equals("FINISHED")) return s;
		if (s.equals("FAILED")) return s;
		return null;
	}

	private static <E> E
	nullGet(E[] array, int offset)
	{
		if (offset >= array.length) return null;
		return array[offset];
	}

//	---%-@-%---
	
	public
	FileLog()
	throws IOException
	{
		//long unixTime = ZonedDateTime.now().toEpochSecond();
		//logFileName = "kawanami-" + unixTime + ".log";
		logFileName = "kawanami.log";

		w = new FileWriter(logFileName, true);
	}

}