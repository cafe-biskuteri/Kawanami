
package cafe.biskuteri.kawanami;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import cafe.biskuteri.hinoki.Tree;
import cafe.biskuteri.hinoki.DSVTokeniser;

public class
FileJobInputs {

	private String
	jobInputsFileName;

	private FileWriter
	w;
	
//	---%-@-%---

	public void
	acceptKawanamiJobInput(IOServices.JobInput jobInput)
	throws IOException
	{
		w.write(toString(jobInput));
		w.flush();
	}

	public List<IOServices.JobInput>
	getKawanamiJobInputs()
	throws IOException
	{
		List<IOServices.JobInput> returnee = new ArrayList<>();
		DSVTokeniser.Options o = new DSVTokeniser.Options();
		FileReader r = new FileReader(jobInputsFileName);
		while (true)
		{
			Tree<String> line = DSVTokeniser.tokenise(r, o);

			if (line.get(0).value.equals(o.endOfStreamValue))
				break;

			IOServices.JobInput addee = new IOServices.JobInput();

			addee.jobId = line.get(0).value;

			addee.serialParams = new HashMap<>();
			for (int o2 = 1; o2 < line.size(); ++o2)
			{
				Tree<String> field = line.get(o2);
				String key = nullGet(field, 0);
				String value = nullGet(field, 1);
				addee.serialParams.put(key, value);
			}

			returnee.add(addee);
		}
		return returnee;
	}

	public void
	close()
	throws IOException
	{ w.close(); }

//	 -	-%-	 -

	private static String
	toString(IOServices.JobInput jobInput)
	{
		StringBuilder b = new StringBuilder();
		b.append(jobInput.jobId);
		for (String key: jobInput.serialParams.keySet())
		{
			String value = jobInput.serialParams.get(key);
			b.append(":[" + key + ":" + value + "]");
		}
		b.append("\n");
		return b.toString();
	}

	private static <E> E
	nullGet(Tree<E> tree, int offset)
	{
		if (offset >= tree.size()) return null;
		return tree.get(offset).value;
	}

//	---%-@-%---
	
	public 
	FileJobInputs()
	throws IOException
	{
		//long unixTime = ZonedDateTime.now().toEpochSecond();
		//jobInputsFileName = "kawanami-" + unixTime + ".inputs";
		jobInputsFileName = "kawanami.inputs";

		w = new FileWriter(jobInputsFileName, true);
	}

}