
package cafe.biskuteri.kawanami;

import java.util.List;
import java.util.Collections;
import java.io.IOException;

class
FullFileIO implements Kawanami.IO {

	private boolean
	crashed;

	private FileLog
	log;

	private FileJobInputs
	jobInputs;

//	---%-@-%---
	
	public void
	acceptKawanamiLogLine(IOServices.LogLine logLine)
	{
		try
		{
			log.acceptKawanamiLogLine(logLine);
		}
		catch (IOException eIo)
		{
			eIo.printStackTrace(System.err);
			crashed = true;
		}
	}

	public void
	acceptKawanamiJobInput(IOServices.JobInput jobInput)
	{
		try
		{
			jobInputs.acceptKawanamiJobInput(jobInput);
		}
		catch (IOException eIo)
		{
			eIo.printStackTrace(System.err);
			crashed = true;
		}
	}

	public List<IOServices.LogLine>
	getKawanamiLog()
	{
		try
		{
			return log.getKawanamiLog();
		}
		catch (IOException eIo)
		{
			eIo.printStackTrace(System.err);
			crashed = true;
			return Collections.emptyList();
			// Sure?
		}
	}

	public List<IOServices.JobInput>
	getKawanamiJobInputs()
	{
		try
		{
			return jobInputs.getKawanamiJobInputs();
		}
		catch (IOException eIo)
		{
			eIo.printStackTrace(System.err);
			crashed = true;
			return Collections.emptyList();
			// Sure?
		}
	}

//	---%-@-%---

	public
	FullFileIO()
	throws IOException
	{
		log = new FileLog();
		jobInputs = new FileJobInputs();
		crashed = false;
	}

}