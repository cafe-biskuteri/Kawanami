
package cafe.biskuteri.kawanami;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.time.ZonedDateTime;


public class
IOServices {

	private Kawanami.IO
	io;

//	---%-@-%---

	public LogLine
	makeLogLine(Kawanami.Job<?> job, String lineDescription)
	{
		LogLine returnee = new LogLine();
		returnee.timestamp = ZonedDateTime.now();
		returnee.jobStatus = job.status;
		returnee.codeClassName = job.code
			.getClass().getSimpleName();
		returnee.jobId = job.id;
		returnee.lineDescription = lineDescription;
		return returnee;
	}

	public JobInput
	makeJobInput(Kawanami.Job<?> job)
	{
		JobInput returnee = new JobInput();
		returnee.jobId = job.id;
		returnee.serialParams = job.controller
			.serialiseJobParams();
		return returnee;
	}

	public List<IncompleteJob>
	findIncompleteJobs()
	{
		Map<String, LogLine> latestLogLines = new HashMap<>();
		Map<String, String> latestJobStatuses = new HashMap<>();
		for (LogLine logLine: io.getKawanamiLog())
		{
			latestJobStatuses.put(logLine.jobId, logLine.jobStatus);
			latestLogLines.put(logLine.jobId, logLine);
		}

		Map<String, JobInput> jobInputs = new HashMap<>();
		for (JobInput jobInput: io.getKawanamiJobInputs())
		{
			jobInputs.put(jobInput.jobId, jobInput);
		}

		List<IncompleteJob> returnee = new ArrayList<>();
		for (String jobId: latestJobStatuses.keySet())
		{
			String latestJobStatus = latestJobStatuses.get(jobId);
			if (latestJobStatus.equals("FINISHED")) continue;

			LogLine latestLogLine = latestLogLines.get(jobId);
			JobInput jobInput = jobInputs.get(jobId);

			IncompleteJob addee = new IncompleteJob();
			addee.jobId = jobId;
			addee.codeClassName = latestLogLine.codeClassName;
			addee.serialParams = jobInput.serialParams;
			returnee.add(addee);
		}
		return returnee;
	}

//	---%-@-%---

	public static class
	LogLine {

		ZonedDateTime
		timestamp;

		String
		jobStatus;

		String
		codeClassName;

		String
		jobId;

		String
		lineDescription;

	}

	public static class
	JobInput {

		String
		jobId;

		Map<String, String>
		serialParams;

	}

	public static class
	IncompleteJob {

		public String
		jobId;

		public String
		codeClassName;

		public Map<String, String>
		serialParams;

	}

//	---%-@-%---

	public
	IOServices(Kawanami.IO io)
	{
		this.io = io;
	}

}
